import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ListView, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Header from './Header'
// import {ListView} from "react-native/Libraries/Lists/ListView/ListView";
// import LinearGradient from 'react-native-linear-gradient'


var DATA = [
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'anhnoibat_ccpk.jpg', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'},
    {image: 'download.png', name: 'Mai Hoang Hai', sdt: '0123456789'}

];
export default class ListUser extends React.Component {
    constructor(){
        super();
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(DATA)
        }
    }
    createRow(property){
        return(
            <View style={{flexDirection: 'row', paddingLeft: 15, paddingTop: 10}}>
                <View style={{marginBottom: 5,flex: 2}}>
                    <Image source={require(`../assets/anhnoibat_ccpk.jpg`)} style={styles.img}/>
                </View>
                <View style={{height: 55 ,flex: 6, paddingTop: 5}}>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{fontSize: 17, color: '#333', fontWeight: '400', flex: 3}} numberOfLines={1} ellipsizeMode={'tail'}>{property.name}</Text>
                    </View>
                    <Text style={{color: '#3b3b3b', marginTop: 5}}>{property.sdt}</Text>
                </View>
                <Icon name="comment-o" style={styles.row_icon}></Icon>
                <Icon name="phone" style={styles.row_icon}></Icon>
                <Icon name="video-camera" style={styles.row_icon}></Icon>
            </View>
        );
    }
    render() {
        return (
            <View style={styles.container}>
                {/*<View style={styles.header}>*/}
                    {/*<Icon name="search" style={{flex: 1, color: 'white', fontSize: 25, paddingTop: 5, paddingLeft: 8}}></Icon>*/}
                    {/*<Text style={{color: 'white', textAlign: 'right', flex: 5, paddingTop: 8, fontSize: 14, paddingRight: 10, fontWeight: 'bold'}}>Danh bạ  </Text>*/}
                {/*</View>*/}
                <Header></Header>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={this.createRow}
                />
            </View>


        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    } ,
    // header: {
    //     height: 55,
    //     backgroundColor: '#13ACFA',
    //     color: 'white',
    //     paddingTop: 20,
    //     flexDirection: 'row',
    //     // marginBottom: 15
    // },
    img: {
        width: 52,
        height: 52,
        borderRadius: 26
    },
    bottom: {
        height: 50,
        flexDirection: 'row',
        borderTopWidth: 0.5,
        borderTopColor: '#b3b3b3',
        paddingTop: 5,
        backgroundColor: '#f5f5f5'
    },
    row_icon: {
        flex: 1,
        fontSize:22,
        paddingTop: 8,
        color: '#333'

    }
});
