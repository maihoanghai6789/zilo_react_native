import React from 'react'
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';

import {withNavigation} from  'react-navigation'



 export default class Footer extends React.Component{
    render(){
        return(
            <View style={styles.bottom} >
                <TouchableOpacity style={{flex: 1}} style={{flex: 1}} onPress={() => this.props.navigation.navigate('Home')}>
                    <Icon name="commenting" style={{ textAlign: 'center', fontSize: 30, color: '#016dff'}}></Icon>
                    <Text style={{textAlign: 'center',color: '#016dff'}}>Chat</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate('Contact')}>
                    <Icon name="address-book" style={{ textAlign: 'center', fontSize: 30, color: '#757575'}}></Icon>
                    <Text style={{textAlign: 'center'}}>Danh bạ </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flex: 1}} style={{flex: 1}} onPress={() => this.props.navigation.navigate('ListGroupUser')}>
                    <Icon name="users" style={{ textAlign: 'center', fontSize: 30, color: '#757575'}}></Icon>
                    <Text style={{textAlign: 'center'}}>Nhóm </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flex: 1}} style={{flex: 1}} onPress={() => this.props.navigation.navigate('News')}>
                    <Icon name="newspaper-o" style={{ textAlign: 'center', fontSize: 30, color: '#757575'}}></Icon>
                    <Text style={{textAlign: 'center'}}>Bảng tin </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flex: 1}} style={{flex: 1}} onPress={() => this.props.navigation.navigate('Profile')}>
                    <Icon name="user-circle-o" style={{ textAlign: 'center', fontSize: 30, color: '#757575'}}></Icon>
                    <Text style={{textAlign: 'center'}}>Cá nhân </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

// export default withNavigation(Footer)

const styles = StyleSheet.create({
    bottom: {
        height: 50,
        flexDirection: 'row',
        borderTopWidth: 0.5,
        borderTopColor: '#b3b3b3',
        paddingTop: 5,
        backgroundColor: '#f5f5f5'
    }
});
