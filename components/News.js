import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ListView, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImageNewsFeed from './ImageNewsFeed'
import Header from './Header'
// import LinearGradient from 'react-native-linear-gradient'


var DATA = [
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'anhnoibat_ccpk.jpg', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'},
    {image: 'download.png', name: 'Mai Hoang Hai', time: '3 giờ trước', content: '450 triệu bán đất trung tâm xã thanh phú - mặt đường tỉnh lộdiện tích768'}

];
export default class ListUser extends React.Component {
    constructor(){
        super();
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(DATA)
        }
    }
    createRow = (property) => {
        return(
            <View style={{backgroundColor: '#ededed'}}>
                <View style={{paddingBottom: 10, paddingLeft: 1, paddingRight: 1, borderBottomWidth: 0.5, borderBottomColor: '#dbdbdb', marginBottom: 10, paddingTop: 10, backgroundColor: '#fff'}}>
                    <View style={{flexDirection: 'row', paddingLeft: 10}}>
                        <Image source={require(`../assets/download.png`)} style={styles.img}/>
                        <View style={{flex: 8, paddingLeft: 10}}>
                            <Text style={{fontSize: 15, fontWeight: '400',color: '#333'}}>{property.name}</Text>
                            <Text style={{fontSize: 12, color: '#adadad'}}>{property.time}</Text>
                        </View>
                    </View>
                    {/*<Text style={{marginTop: 10, marginBottom: 10, fontSize: 20}}>{property.content}</Text>*/}
                    <Text style={{marginTop: 10, marginBottom: 10, paddingLeft: 10, paddingRight: 10, fontSize: 14, color: '#333'}}>{property.content}</Text>

                    {/*<View style={{height: 200}}>*/}
                        {/*<Image style={{height: 200, flex: 1}} source={require(`../assets/395_Detroit_St.25_forprintuse.0.jpg`)} />*/}
                    {/*</View>*/}
                    <ImageNewsFeed/>
                    <View style={{flexDirection: 'row', marginTop:10, marginLeft: 10, marginRight: 10, borderTopWidth: 0.5, borderTopColor: '#dbdbdb', paddingTop: 7}}>
                        <TouchableOpacity style={styles.interactive}>
                            <Icon name="heart-o" style={styles.interactive_icon}></Icon>
                            <Text style={styles.interactive_text}>Lưu tin</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.interactive} onPress={() => this.props.navigation.navigate('ChatBox')}>
                            <Icon name="comment-o" style={styles.interactive_icon}></Icon>
                            <Text style={styles.interactive_text}>Chat ngay </Text>
                        </TouchableOpacity>

                        <View style={{flex: 2}}>

                        </View>
                    </View>
                </View>
            </View>
        );
    }
    render() {
        return (
            <View style={styles.container}>

                {/*<View style={styles.header}>*/}
                    {/*<Icon name="search" style={{flex: 1, color: 'white', fontSize: 25, paddingTop: 5, paddingLeft: 8}}></Icon>*/}
                    {/*<Text style={{color: 'white', textAlign: 'right', flex: 5, paddingTop: 8, fontSize: 14, paddingRight: 10, fontWeight: 'bold'}}>Tin tức nhà đất mới nhất trong ngày </Text>*/}
                {/*</View>*/}
                <Header></Header>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={this.createRow}
                />

            </View>


        );
    }

}

const styles = StyleSheet.create({
    container: {

        flex: 1,
        // paddingLeft: 5,
        // paddingRight: 5
    } ,
    // header: {
    //     height: 55,
    //     backgroundColor: '#13ACFA',
    //     color: 'white',
    //     paddingTop: 20,
    //     flexDirection: 'row',
    //     // marginBottom: 15
    // },
    img: {
        width: 40,
        height: 40,
        borderRadius: 19,
        flex: 1
    },
    bottom: {
        height: 50,
        flexDirection: 'row',
        borderTopWidth: 0.5,
        borderTopColor: '#b3b3b3',
        paddingTop: 5,
        backgroundColor: '#f5f5f5'
    },
    interactive: {
        flex: 2, flexDirection: 'row'
    },
    interactive_icon: {
        flex: 1,
        fontSize: 20,
        color: '#333'

    },
    interactive_text: {
        flex: 3,
        paddingTop: 3,
        fontSize: 15,
        color: '#333'

    }
});
