import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ListView, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Header from './Header'

export default class Profile extends React.Component{
    render() {
        return(
            <View>
                {/*<View style={styles.header}>*/}
                    {/*<Icon name="search" style={{flex: 1, color: 'white', fontSize: 25, paddingTop: 5, paddingLeft: 8}}></Icon>*/}
                    {/*<Text style={{color: 'white', textAlign: 'right', flex: 5, paddingTop: 8, fontSize: 14, paddingRight: 10, fontWeight: 'bold'}}>Thông tin cá nhân </Text>*/}
                {/*</View>*/}

                <Header></Header>
                <View style={{ alignItems: 'center', paddingTop: 50, paddingLeft: 15}}>
                    <Image source={require(`../assets/anhnoibat_ccpk.jpg`)} style={styles.img}/>
                    <View style={styles.li_info}>
                        <Icon name="user-circle-o" style={styles.li_info_icon_1}></Icon>
                        <Text style={styles.li_info_text}>Tên người dùng </Text>
                        <Text style={styles.li_info_text}>Mai Hoang Hai</Text>

                    </View>
                    <View style={styles.li_info}>
                        <Icon name="phone-square" style={styles.li_info_icon_1}></Icon>
                        <Text style={styles.li_info_text}>Số điện thoại </Text>
                        <Text style={styles.li_info_text}>0123456789</Text>
                    </View>
                    <View style={styles.li_info}>
                        <Icon name="openid" style={styles.li_info_icon_1}></Icon>
                        <Text style={styles.li_info_text}>ID người dùng</Text>
                        <Text style={styles.li_info_text}>12345</Text>
                    </View>
                    <View style={styles.li_info}>
                        <Icon name="th-list" style={styles.li_info_icon_1}></Icon>
                        <Text style={styles.li_info_text}>Danh sách tin đã lưu</Text>
                        <View style={{flex: 5}}></View>
                        <Icon name="chevron-right" style={styles.li_info_icon_2}></Icon>
                    </View>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')} style={styles.li_info}>
                        <Icon name="arrow-circle-right" style={styles.li_info_icon_1}></Icon>
                        <Text style={styles.li_info_text}>Đăng xuất</Text>
                        <View style={{flex: 5}}></View>
                        <Icon name="chevron-right" style={styles.li_info_icon_2}></Icon>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    } ,
    // header: {
    //     height: 55,
    //     backgroundColor: '#13ACFA',
    //     color: 'white',
    //     paddingTop: 20,
    //     flexDirection: 'row',
    //     marginBottom: 15
    // },
    img: {
        width: 170,
        height: 170,
        borderRadius: 30,
        marginBottom: 20
    },
    li_info: {
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: '#dbdbdb',
        height: 35,
        marginBottom: 10

    },
    li_info_icon_1: {
        flex:1,
        fontSize: 27,
        color: '#333'

    },
    li_info_icon_2: {
        flex: 1,
        paddingTop: 5,
        color: '#333'

    },
    li_info_text: {
        flex:5,
        fontSize: 16,
        marginTop: 3,
        color: '#333'

    }
});