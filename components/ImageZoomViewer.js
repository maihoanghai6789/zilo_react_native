import React from 'react'
import {StyleSheet, Modal, View, Text, TouchableOpacity} from 'react-native'
import ImageViewer from 'react-native-image-zoom-viewer'


var images = [];

export default class ImageZoomViewer extends React.Component{
    constructor(props) {
        super(props);
        images = [];
        props.navigation.state.params.arr_img.forEach(function(element){
            if (images.indexOf(`{'url' : '${element}'}`) == -1){
                images.push({'url' : element});
            }
        });
        console.log(images);
        this.state = {
            isModelVisible: true,
        };
    }
    // ShowModalFunction(visible) {
    //     this.setState({ isModelVisible: false });
    //     this.props.navigate.navigation
    // }
    render() {

        const {goBack} = this.props.navigation;
        return (
            <Modal visible={true} transparent={true} style={{position: 'relative'}}>
                <TouchableOpacity onPress={() => goBack()} style={{postition: 'absolute', backgroundColor: '#000', top: 50, zIndex: 100, borderWidth: 1, borderColor: '#fff', padding: 10, width: 75}}>
                    <Text style={{fontSize: 18 ,fontWeight: 'bold', color: '#fff'}}>Đóng </Text>
                </TouchableOpacity>
                <ImageViewer imageUrls={images} index={this.props.navigation.state.params.index}/>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({

})