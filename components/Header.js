import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ListView, Image, TextInput, KeyboardAvoidingView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Header extends React.Component{
    render() {
        return (
            <View style={styles.header}>
                <KeyboardAvoidingView behavior="padding">
                    <View style={{flexDirection: "row"}}>
                        <Icon name="search" style={styles.icon}></Icon>
                        <TextInput
                            placeholder="Nhập từ khóa tìm kiếm "
                            placeholderTextColor={'#e6e6e6'}
                            style={{flex: 6, paddingTop: 8, fontSize: 16, color: '#fff'}}
                        />
                        <Icon name="bell-o" style={styles.icon}></Icon>
                    </View>
                </KeyboardAvoidingView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
   header: {
       height: 62,
       backgroundColor: '#13ACFA',
       color: 'white',
       paddingTop: 20,
       // flexDirection: 'row',
       paddingLeft: 15
   },
   icon: {
       flex: 1,
       color: '#fff',
       fontSize: 25,
       paddingTop: 5
   }

});