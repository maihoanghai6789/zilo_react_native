import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ListView, Image, Button } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';


export default class Login extends React.Component {
    render(){
        return(
            <View style={{alignItems: 'center', marginTop: '45%'}}>
                <Image source={require(`../assets/logo.png`)} style={{marginBottom: 15}}></Image>
                <View style={{backgroundColor: 'blue', height: 45, width: '90%', flexDirection: 'row', borderRadius: 5, paddingTop: 6, marginTop:10}}>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <Icon name="facebook" style={{fontSize: 30, color: '#ffffff'}}></Icon>
                    </View>
                    <Text style={{flex: 6, color: '#ffffff', fontSize: 17, fontWeight: 'bold', marginTop: 3}}>Đăng nhập với tài khoản facebook</Text>
                </View>
                <View style={{backgroundColor: 'red', height: 45, width: '90%', flexDirection: 'row', borderRadius: 5, paddingTop: 6, marginTop:10}}>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <Icon name="google-plus" style={{fontSize: 30, color: '#ffffff', marginTop: 3}}></Icon>
                    </View>
                    <Text style={{flex: 6, color: '#ffffff', fontSize: 17, fontWeight: 'bold', marginTop: 3}}>Đăng nhập với tài khoản google</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

});