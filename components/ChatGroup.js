import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ListView, Image, TextInput, KeyboardAvoidingView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

var DATA = [
    {image: 'download.png', mess: 'Specifies autocomplete hints for the system, so it can provide autofill', time: '08:30', obj: 'they'},
    {image: 'download.png', mess: 'Specifies autocomplete hints for the system, so it can provide autofill', time: '08:30',obj: 'you'},
    {image: 'download.png', mess: 'Specifies autocomplete ', time: '08:30',obj: 'they'},
    {image: 'download.png', mess: 'Specifies autocomplete hints for the system, so it can provide autofill', time: '08:30',obj: 'they'},
    {image: 'download.png', mess: 'Specifies autocomplete hints for the system, so it can provide autofill', time: '08:30',obj: 'they'},
    {image: 'download.png', mess: 'Specifies ', time: '08:30',obj: 'they'},
    {image: 'download.png', mess: 'Specifies autocomplete hints for the system, so it can provide autofill', time: '08:30',obj: 'they'},
    {image: 'download.png', mess: 'Specifies autocomplete hints for the system, so it can provide autofill', time: '08:30',obj: 'they'},
    {image: 'download.png', mess: 'Specifies autocomplete hints for the system, so it can provide autofill', time: '08:30',obj: 'they'},
    {image: 'download.png', mess: 'Specifies autocomplete ', time: '08:30',obj: 'you'},
    {image: 'download.png', mess: 'Specifies autocomplete hints for the system', time: '08:30',obj: 'you'},
    {image: 'download.png', mess: 'Specifies autocomplete hints for the system, so it can provide autofill', time: '08:30',obj: 'they'},
    {image: 'download.png', mess: 'Specifies autocomplete ', time: '08:30',obj: 'they'}
];

export default class ChatBox extends React.Component {

    constructor(props){
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(DATA),
            displayOption: false
        }
    }
    createRowChat(property){
        if (property.obj == "they"){
            return(
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <View style={{flex: 1, paddingLeft: 5}}>
                        <Image style={{width: 35, height: 35, borderRadius: 17}} source={require(`../assets/anhnoibat_ccpk.jpg`)}></Image>
                    </View>
                    <View style={{flex: 7}}>
                        <View style={styles.theirmess}>
                            <Text>{property.mess}</Text>
                            <Text style={styles.timechat}>{property.time}</Text>
                        </View>
                    </View>
                    <View style={{flex: 1}}>
                    </View>
                </View>

            );
        }
        else {
            return(
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <View style={{flex: 1}}>
                    </View>

                    <View style={{flex: 7}}>
                        <View style={styles.yourmess}>
                            <Text>{property.mess}</Text>
                            <Text style={styles.timechat}>{property.time}</Text>
                        </View>
                    </View>
                    {/*<View style={{flex: 1}}>*/}
                    {/*</View>*/}
                </View>
            );
        }
    }
    clickOptions(){
        this.setState({
            displayOption: !this.state.displayOption
        });
    }
    displayOption(){
        if (this.state.displayOption ==  true){
            return(
                <TouchableOpacity style={{position: 'absolute' ,height: '100%', width: '100%', zIndex: 100}} onPress={() => this.clickOptions()}>
                    <TouchableOpacity style={{position: 'absolute', top: 0, right: 10, width: 130, height: 45, backgroundColor: '#bbb6be', borderRadius: 10, borderWidth: 2, borderColor: '#adadad'}} onPress={() => this.props.navigation.navigate('ListGroupUser')}>
                        <Text style={{color: 'red', textAlign: 'center', paddingTop: 12, fontSize: 15}}>Rời khỏi nhóm !</Text>
                    </TouchableOpacity>
                </TouchableOpacity>

            );
        } else{
            return null;
        }
    }

    render() {
        const {goBack} = this.props.navigation
        // if (this.state.displayOption == true){
        //     text = <Text>Roi nhom</Text>;
        // }
        return (
            <View style={styles.container}>

                <View style={styles.header}>
                    <Icon name="chevron-left" style={{flex: 1, color: 'white', fontSize: 25, paddingTop: 8, paddingLeft: 8}} onPress={() => goBack()}></Icon>
                    <View style={{flex: 5}}>
                        <Text style={{color: 'white',  flex: 3, paddingTop: 5, fontSize: 17, paddingRight: 10, fontWeight: 'bold'}} numberOfLines={1} ellipsizeMode={'tail'}>quận Hai Bà Trưng, Hà Nội</Text>
                        <Text style={{flex: 2, fontSize: 11, color: '#c9c9c9'}}>Vừa mới truy cập</Text>
                    </View>
                    <Icon name="phone" style={styles.icon_header}></Icon>
                    <Icon name="video-camera" style={styles.icon_header}></Icon>
                    <Icon name="ellipsis-h" style={styles.icon_header} onPress={() => {this.clickOptions()}}></Icon>
                </View>

                <View style={styles.main}>
                    {this.displayOption()}
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this.createRowChat}
                    />

                </View>

                <KeyboardAvoidingView behavior="padding">
                    <View style={styles.bottom}>
                        <Icon name="smile-o" style={styles.icon}></Icon>
                        <Icon name="image" style={styles.icon}></Icon>
                        <TextInput style={styles.textinput}>

                        </TextInput>
                        <Icon name="location-arrow" style={styles.icon}></Icon>
                    </View>
                </KeyboardAvoidingView>
            </View>


        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative'
    } ,
    header: {
        height: 62,
        // flex: 1,
        backgroundColor: '#13ACFA',
        color: 'white',
        paddingTop: 20,
        flexDirection: 'row',
        // position: 'relative'

    },
    icon_header: {
        flex: 1,
        color: 'white',
        fontSize: 25,
        paddingTop: 7,
        paddingLeft: 8,
        textAlign: 'center'

    },
    main: {
        flex: 1,
        backgroundColor: '#E3E0E3',
        paddingTop: 5,
        // zIndex: 99,
        position: 'relative'
    },
    bottom: {
        height: 45,
        // flex: 1,
        borderTopWidth: 0.5,
        borderTopColor: 'gray',
        backgroundColor: 'white',
        fontSize: 16,
        flexDirection: 'row'
    },
    textinput: {
        // backgroundColor: 'red',
        // marginTop: 7,
        height: 45,
        flex: 8,
        paddingLeft: 5,
        fontSize: 16
    },
    icon: {
        flex: 1,
        fontSize: 27,
        textAlign: 'center',
        paddingTop: 7
    },
    theirmess: {
        borderWidth: 0.5,
        borderRadius: 12,
        borderColor: '#adadad',
        padding: 8,
        backgroundColor: '#ffffff',
        // width: 'auto'
        // maxWidth: 300,
        justifyContent: 'center'


    },
    yourmess: {
        borderWidth: 0.5,
        borderRadius: 12,
        borderColor: '#adadad',
        padding: 8,
        backgroundColor: '#DAE9FD',
        marginRight: 5
    },
    timechat: {
        color: '#b0b0b0',
        fontSize: 11,
        marginTop: 5
    }
    // form: {
    //     justifyContent: 'space-between'
    // }
});