import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ListView, Image } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import {withNavigation} from 'react-navigation'

var arr_img = [
    'https://vneconomy.mediacdn.vn/zoom/480_270/2017/bat-dong-san-1510133129977-crop-15554704591581821911012.jpg',
    'https://avatars2.githubusercontent.com/u/7970947?v=3&s=460',
    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/sample_img.png',
    'https://vneconomy.mediacdn.vn/zoom/480_270/2017/bat-dong-san-1510133129977-crop-15554704591581821911012.jpg'

];
var count_img = arr_img.length;
 class ImageNewsFeed extends React.Component{
    constructor(props){

        super(props);
        // const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        // this.state = {
        //     dataSource: ds.cloneWithRows(DATA)
        // }
    }
    list_view_img =() =>{
        if (count_img == 0) return null;
        else if (count_img == 1) {
            console.log(arr_img[0]);
            return(
                <View style={{flex: 1}}>
                    <Image style={{width: '100%', height: 200}} source={{uri: arr_img[0]}}/>
                </View>
            );
        }
        else if (count_img == 2) {
            return(
                <View style={{flexDirection: 'row'}}>
                    <Image source={{uri: arr_img[0]}} style={{flex: 1, height: 200, width: '100%'}}/>
                    <Image source={{uri: arr_img[1]}} style={{flex: 1, marginLeft: 3, height: 200, width: '100%'}}/>
                </View>
            )
        }
        else if (count_img == 3){
            return(
                <View style={{flexDirection: 'row'}}>
                    <Image source={{uri: arr_img[0]}} style={{flex: 2, height: 200, marginRight: 3}}/>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Image source={{uri: arr_img[1]}} style={{flex: 1}}/>
                        <Image source={{uri: arr_img[2]}} style={{flex: 1, marginTop: 3}}/>
                    </View>
                </View>
            )
        }
        else {
            return(
                <View style={{flexDirection: 'row' }}>
                    <TouchableOpacity style={{flex: 2, marginRight: 3}}  onPress={() => {this.props.navigation.navigate('ImageZoomViewer', {arr_img: arr_img, index : 0})}}>
                        <Image source={{uri: arr_img[0]}} style={{ height: 200,width: '100%'}}/>

                    </TouchableOpacity>
                    <View style={{flex: 1}}>
                        <TouchableOpacity style={{flex: 1}}  onPress={() => {this.props.navigation.navigate('ImageZoomViewer', {arr_img: arr_img, index : 1})}}>
                            <Image source={{uri: arr_img[1]}} style={{width: '100%', height: '100%'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex: 1, marginTop: 3}} onPress={() => {this.props.navigation.navigate('ImageZoomViewer', {arr_img: arr_img, index : 2})}}>
                            <View style={{flex: 1, position: 'relative'}}>
                                <View style={{position: 'absolute', width: '100%', height: '100%', opacity: 0.3, backgroundColor: 'black', zIndex: 2}}></View>
                                <Image source={{uri: arr_img[2]}} style={{width: '100%', height: '100%', zIndex: 1}}/>
                                <Text style={{position: 'absolute', top: '30%',left: '35%', fontSize: 23, color: '#ffffff', zIndex: 3}}>+{count_img - 2}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }


    }
    render() {
        return(
            <View style={{flex: 1 ,height: 200}}>
                {this.list_view_img()}
            </View>
        );
    }
}
export default withNavigation(ImageNewsFeed)