import React from 'react';
import {StyleSheet, Text, View, Image, Navigator, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
// import IconMat from 'react-native-vector-icons/MaterialCommunityIcons'
// import IconEi from 'react-native-vector-icons/EvilIcons'
// import IconFont5 from 'react-native-vector-icons/FontAwesome5'
import { createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import ListUser from './components/ListUser'
import ListGroupUser from './components/ListGroupUser'
import ChatBox from './components/ChatBox'
import Contact from './components/Contact'
import News from './components/News'
import Login from './components/Login'
import Profile from './components/Profile'
import ChatGroup from './components/ChatGroup'
import ImageZoomViewer from './components/ImageZoomViewer'
import {createMaterialBottomTabNavigator} from "react-navigation-material-bottom-tabs";


const TabNavigator = createMaterialBottomTabNavigator(
    {
        ListUser: {
            screen: ListUser,
            navigationOptions: {
                tabBarLabel: 'Chat',
                tabBarIcon: ({tintColor}) => (
                    <View style={{position: 'relative'}}>
                        <Icon name={"commenting-o"} style={[{color: tintColor}]} size={23}></Icon>
                        <View style={{width: 17, height: 15, backgroundColor: 'red', borderRadius: 10, position: 'absolute', right: -13, borderColor: '#ffffff', borderWidth: 1}}>
                            <Text style={{fontSize: 10, color: '#ffffff', textAlign: 'center', fontWeight: 'bold', paddingTop: 0}}>10</Text>
                        </View>

                    </View>
                ),
            }
        },
        ListGroupUser: {
            screen: ListGroupUser,
            navigationOptions: {
                tabBarLabel: 'Nhóm ',
                tabBarIcon: ({tintColor}) => (
                    <View style={{position: 'relative'}}>
                        {/*<IconMat name={"account-group-outline"} style={{color: tintColor}} size={30}></IconMat>*/}
                        <Icon name={"group"} style={[{color: tintColor}]} size={23 }></Icon>
                        <View style={{width: 17, height: 15, backgroundColor: 'red', borderRadius: 10, position: 'absolute', right: -13, borderColor: '#ffffff', borderWidth: 1}}>
                            <Text style={{fontSize: 10, color: '#ffffff', textAlign: 'center', fontWeight: 'bold', paddingTop: 0}}>5</Text>
                        </View>
                    </View>
                ),
            }
        },
        News: {
            screen: News,
            navigationOptions: {
                tabBarLabel: 'Bảng tin ',
                tabBarIcon: ({tintColor}) => (
                    <View>
                        <Icon name={"newspaper-o"} style={{color: tintColor}} size={23}></Icon>
                    </View>
                ),
            }
        },
        Contact: {
            screen: Contact,
            navigationOptions: {
                tabBarLabel: 'Danh bạ',
                tabBarIcon: ({tintColor}) => (
                    <View>
                        <Icon name={"address-book-o"} style={[{color: tintColor}]} size={23}></Icon>
                    </View>
                ),
            }
        },
        Profile: {
            screen: Profile,
            navigationOptions: {
                tabBarLabel: 'Cá nhân',
                tabBarIcon: ({tintColor}) => (
                    <View>
                        <Icon name={"user-circle-o"} style={[{color: tintColor}]} size={23}></Icon>
                        {/*<IconEi name={"user"} style={[{color: tintColor}]} size={35}></IconEi>*/}
                    </View>
                ),
            }
        },

    },
    {
        initialRouteName: 'News',
        activeTintColor: '#07a8fa',
        barStyle: {backgroundColor: '#ffffff'}
    },
);

const statckNav = createStackNavigator({

    ListNav: {
        screen: TabNavigator
    },
    ChatBox: {
        screen: ChatBox
    },
    ChatGroup: {
        screen: ChatGroup
    },
    Login: {
        screen: Login
    },
    ImageZoomViewer: {
        screen: ImageZoomViewer
    }
},{
    headerMode: 'none'

});

export default createAppContainer(statckNav);


